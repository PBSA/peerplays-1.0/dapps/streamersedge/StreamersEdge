# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.11.2](https://github.com/PBSA/StreamersEdge/compare/v0.11.1...v0.11.2) (2020-03-23)


### Bug Fixes

* resolve challenge even if stream is not live ([0a698e4](https://github.com/PBSA/StreamersEdge/commit/0a698e4))
* strm-1033: handle peerplays connection errors ([#243](https://github.com/PBSA/StreamersEdge/issues/243)) ([202369b](https://github.com/PBSA/StreamersEdge/commit/202369b))
* strm-1084 create payment url and capture payment in backend ([#244](https://github.com/PBSA/StreamersEdge/issues/244)) ([f2612f2](https://github.com/PBSA/StreamersEdge/commit/f2612f2))



### [0.11.1](https://github.com/PBSA/StreamersEdge/compare/v0.11.0...v0.11.1) (2020-02-27)


### Bug Fixes

* change email templates ([#242](https://github.com/PBSA/StreamersEdge/issues/242)) ([5495507](https://github.com/PBSA/StreamersEdge/commit/5495507))
* make cron jobs run on api call ([#241](https://github.com/PBSA/StreamersEdge/issues/241)) ([3863a4a](https://github.com/PBSA/StreamersEdge/commit/3863a4a))



## [0.11.0](https://github.com/PBSA/StreamersEdge/compare/v0.10.7...v0.11.0) (2020-02-24)



### [0.10.7](https://github.com/PBSA/StreamersEdge/compare/v0.10.6...v0.10.7) (2020-02-20)


### Bug Fixes

* ppy password should be min 12 characters long ([#237](https://github.com/PBSA/StreamersEdge/issues/237)) ([57f14de](https://github.com/PBSA/StreamersEdge/commit/57f14de))
* twitchUsername unique constraint violation ([#238](https://github.com/PBSA/StreamersEdge/issues/238)) ([1fe3607](https://github.com/PBSA/StreamersEdge/commit/1fe3607))
* users.map is not a function ([#236](https://github.com/PBSA/StreamersEdge/issues/236)) ([25f6d63](https://github.com/PBSA/StreamersEdge/commit/25f6d63))
* winners.map is not a function, changes in payments job for 1 winner ([#235](https://github.com/PBSA/StreamersEdge/issues/235)) ([38a1c08](https://github.com/PBSA/StreamersEdge/commit/38a1c08))


### Features

* strm-1033: allow multiple peerplays endpoints in config ([#239](https://github.com/PBSA/StreamersEdge/issues/239)) ([839e39e](https://github.com/PBSA/StreamersEdge/commit/839e39e))



### [0.10.6](https://github.com/PBSA/StreamersEdge/compare/v0.10.5...v0.10.6) (2020-02-14)


### Bug Fixes

* account for pubg match to complete ([#230](https://github.com/PBSA/StreamersEdge/issues/230)) ([411410b](https://github.com/PBSA/StreamersEdge/commit/411410b))
* shift payout amount for blockchain precision ([#231](https://github.com/PBSA/StreamersEdge/issues/231)) ([1da79b3](https://github.com/PBSA/StreamersEdge/commit/1da79b3))



### [0.10.5](https://github.com/PBSA/StreamersEdge/compare/v0.10.4...v0.10.5) (2020-02-14)


### Bug Fixes

* corrected error details STRM-1083 ([#227](https://github.com/PBSA/StreamersEdge/issues/227)) ([d40b8e3](https://github.com/PBSA/StreamersEdge/commit/d40b8e3))
* cors error on redirecting to paypal from backend ([#225](https://github.com/PBSA/StreamersEdge/issues/225)) ([500b1cd](https://github.com/PBSA/StreamersEdge/commit/500b1cd))
* return joined as false if user is not logged in STRM-1063 ([#228](https://github.com/PBSA/StreamersEdge/issues/228)) ([c925d0b](https://github.com/PBSA/StreamersEdge/commit/c925d0b))
* youtube connection error STRM-1025 ([#226](https://github.com/PBSA/StreamersEdge/issues/226)) ([6e5c3b4](https://github.com/PBSA/StreamersEdge/commit/6e5c3b4))



### [0.10.4](https://github.com/PBSA/StreamersEdge/compare/v0.10.3...v0.10.4) (2020-02-11)


### Bug Fixes

* allow donation to live challenges ([#221](https://github.com/PBSA/StreamersEdge/issues/221)) ([c511429](https://github.com/PBSA/StreamersEdge/commit/c511429))
* changing limit of report SE-53 ([#220](https://github.com/PBSA/StreamersEdge/issues/220)) ([883daa5](https://github.com/PBSA/StreamersEdge/commit/883daa5))
* paypal auth redirect url error and session not saved ([#219](https://github.com/PBSA/StreamersEdge/issues/219)) ([543dad8](https://github.com/PBSA/StreamersEdge/commit/543dad8))
* strm-1049: getStreamForUser fix ([#224](https://github.com/PBSA/StreamersEdge/issues/224)) ([6630d7b](https://github.com/PBSA/StreamersEdge/commit/6630d7b))



### [0.10.3](https://github.com/PBSA/StreamersEdge/compare/v0.10.2...v0.10.3) (2020-02-04)


### Bug Fixes

* strm-1049: cron job fixes ([#218](https://github.com/PBSA/StreamersEdge/issues/218)) ([cdc30b0](https://github.com/PBSA/StreamersEdge/commit/cdc30b0))



### [0.10.2](https://github.com/PBSA/StreamersEdge/compare/v0.10.1...v0.10.2) (2020-01-31)


### Bug Fixes

* banned user shouldn't be able to create or donate-to challenge ([#216](https://github.com/PBSA/StreamersEdge/issues/216)) ([190f27e](https://github.com/PBSA/StreamersEdge/commit/190f27e))
* strm-1036 add facebook link in user profile ([#213](https://github.com/PBSA/StreamersEdge/issues/213)) ([4036112](https://github.com/PBSA/StreamersEdge/commit/4036112))
* strm-1036 populate facebooklink when linking to existing account ([#214](https://github.com/PBSA/StreamersEdge/issues/214)) ([ff33a42](https://github.com/PBSA/StreamersEdge/commit/ff33a42))
* strm-1057 banned user shouldn't be able to login with social media ([#215](https://github.com/PBSA/StreamersEdge/issues/215)) ([e3db32e](https://github.com/PBSA/StreamersEdge/commit/e3db32e))



### [0.10.1](https://github.com/PBSA/StreamersEdge/compare/v0.10.0...v0.10.1) (2020-01-29)


### Bug Fixes

* strm-1030: games job fixes ([#211](https://github.com/PBSA/StreamersEdge/issues/211)) ([53dc912](https://github.com/PBSA/StreamersEdge/commit/53dc912))
* strm-1038: fix check in refund challenge ([#212](https://github.com/PBSA/StreamersEdge/issues/212)) ([1fe8640](https://github.com/PBSA/StreamersEdge/commit/1fe8640))
* strm-933: do not list paid challenges in challenge api ([#207](https://github.com/PBSA/StreamersEdge/issues/207)) ([be5d2b7](https://github.com/PBSA/StreamersEdge/commit/be5d2b7))



## [0.10.0](https://github.com/PBSA/StreamersEdge/compare/v0.7.0...v0.10.0) (2020-01-10)


### Bug Fixes

* fixing report and ban api ([#209](https://github.com/PBSA/StreamersEdge/issues/209)) ([6502fbc](https://github.com/PBSA/StreamersEdge/commit/6502fbc))
* remove extra parameter passed to query and twitch stream error ([#205](https://github.com/PBSA/StreamersEdge/issues/205)) ([fa0c1eb](https://github.com/PBSA/StreamersEdge/commit/fa0c1eb))
* strm-878: set default value for users userType ([#204](https://github.com/PBSA/StreamersEdge/issues/204)) ([a42f462](https://github.com/PBSA/StreamersEdge/commit/a42f462))
* strm-965: admin user search validation fix ([#206](https://github.com/PBSA/StreamersEdge/issues/206)) ([44ee8a2](https://github.com/PBSA/StreamersEdge/commit/44ee8a2))


### Features

* add searching in admin/report api ([#208](https://github.com/PBSA/StreamersEdge/issues/208)) ([f7fa913](https://github.com/PBSA/StreamersEdge/commit/f7fa913))
* strm-876: games job changes ([#200](https://github.com/PBSA/StreamersEdge/issues/200)) ([5cf6f8c](https://github.com/PBSA/StreamersEdge/commit/5cf6f8c))



## [0.9.0](https://github.com/PBSA/StreamersEdge/compare/v0.7.0...v0.9.0) (2020-01-06)


### Bug Fixes

* remove extra parameter passed to query and twitch stream error ([#205](https://github.com/PBSA/StreamersEdge/issues/205)) ([fa0c1eb](https://github.com/PBSA/StreamersEdge/commit/fa0c1eb))
* strm-878: set default value for users userType ([#204](https://github.com/PBSA/StreamersEdge/issues/204)) ([a42f462](https://github.com/PBSA/StreamersEdge/commit/a42f462))
* strm-965: admin user search validation fix ([#206](https://github.com/PBSA/StreamersEdge/issues/206)) ([44ee8a2](https://github.com/PBSA/StreamersEdge/commit/44ee8a2))


### Features

* add searching in admin/report api ([#208](https://github.com/PBSA/StreamersEdge/issues/208)) ([f7fa913](https://github.com/PBSA/StreamersEdge/commit/f7fa913))
* strm-876: games job changes ([#200](https://github.com/PBSA/StreamersEdge/issues/200)) ([5cf6f8c](https://github.com/PBSA/StreamersEdge/commit/5cf6f8c))



## [0.8.0](https://github.com/PBSA/StreamersEdge/compare/v0.7.0...v0.8.0) (2019-12-23)


### Bug Fixes

* strm-878: set default value for users userType ([#204](https://github.com/PBSA/StreamersEdge/issues/204)) ([a42f462](https://github.com/PBSA/StreamersEdge/commit/a42f462))



## [0.7.0](https://github.com/PBSA/StreamersEdge/compare/v0.4.0...v0.7.0) (2019-12-18)


### Bug Fixes

* adding PaypalEmail [#688](https://github.com/PBSA/StreamersEdge/issues/688) ([#197](https://github.com/PBSA/StreamersEdge/issues/197)) ([a9d4186](https://github.com/PBSA/StreamersEdge/commit/a9d4186))
* check pubgusername instead of steamId in the patch profile ([#196](https://github.com/PBSA/StreamersEdge/issues/196)) ([653d62c](https://github.com/PBSA/StreamersEdge/commit/653d62c))
* error in ppy sign up when username exists ([#201](https://github.com/PBSA/StreamersEdge/issues/201)) ([dcc7052](https://github.com/PBSA/StreamersEdge/commit/dcc7052))
* errors in sorting challenges ([#198](https://github.com/PBSA/StreamersEdge/issues/198)) ([bcddcea](https://github.com/PBSA/StreamersEdge/commit/bcddcea))
* filters not working in admin users search ([#194](https://github.com/PBSA/StreamersEdge/issues/194)) ([742bd3b](https://github.com/PBSA/StreamersEdge/commit/742bd3b))
* peerplays transaction amount format wrong ([#191](https://github.com/PBSA/StreamersEdge/issues/191)) ([6c9b127](https://github.com/PBSA/StreamersEdge/commit/6c9b127))
* return newuser field in response on peerplays login ([#199](https://github.com/PBSA/StreamersEdge/issues/199)) ([3bdb9ef](https://github.com/PBSA/StreamersEdge/commit/3bdb9ef))
* return status in challenge details ([#195](https://github.com/PBSA/StreamersEdge/issues/195)) ([c21eb09](https://github.com/PBSA/StreamersEdge/commit/c21eb09))



## [0.6.0](https://github.com/PBSA/StreamersEdge/compare/v0.4.0...v0.6.0) (2019-12-16)


### Bug Fixes

* filters not working in admin users search ([#194](https://github.com/PBSA/StreamersEdge/issues/194)) ([742bd3b](https://github.com/PBSA/StreamersEdge/commit/742bd3b))
* peerplays transaction amount format wrong ([#191](https://github.com/PBSA/StreamersEdge/issues/191)) ([6c9b127](https://github.com/PBSA/StreamersEdge/commit/6c9b127))
* send notification for a new challenge to all subscribed users ([#193](https://github.com/PBSA/StreamersEdge/issues/193)) ([33c047b](https://github.com/PBSA/StreamersEdge/commit/33c047b))
* strm-871: patch profile changes ([#185](https://github.com/PBSA/StreamersEdge/issues/185)) ([33f7b91](https://github.com/PBSA/StreamersEdge/commit/33f7b91))



## [0.5.0](https://github.com/PBSA/StreamersEdge/compare/v0.3.0...v0.5.0) (2019-12-16)


### Bug Fixes

* modifying getChallengesApi [#866](https://github.com/PBSA/StreamersEdge/issues/866) ([#184](https://github.com/PBSA/StreamersEdge/issues/184)) ([acf9448](https://github.com/PBSA/StreamersEdge/commit/acf9448))
* return error route for failure in oauth ([#189](https://github.com/PBSA/StreamersEdge/issues/189)) ([a0b2956](https://github.com/PBSA/StreamersEdge/commit/a0b2956))
* STRM-784 validations for negative id and non-existing user ([#190](https://github.com/PBSA/StreamersEdge/issues/190)) ([01b2513](https://github.com/PBSA/StreamersEdge/commit/01b2513))
* strm-871: patch profile changes ([#185](https://github.com/PBSA/StreamersEdge/issues/185)) ([33f7b91](https://github.com/PBSA/StreamersEdge/commit/33f7b91))
* strm-897 change response for change-email api to match swagger docs ([#188](https://github.com/PBSA/StreamersEdge/issues/188)) ([d88f011](https://github.com/PBSA/StreamersEdge/commit/d88f011))
* strm-900: unset session.newUser on social media login ([#187](https://github.com/PBSA/StreamersEdge/issues/187)) ([c902ca9](https://github.com/PBSA/StreamersEdge/commit/c902ca9))



## [0.4.0](https://github.com/PBSA/StreamersEdge/compare/v0.2.0...v0.4.0) (2019-11-29)


### Bug Fixes

* challenge api logged out [#832](https://github.com/PBSA/StreamersEdge/issues/832) ([#173](https://github.com/PBSA/StreamersEdge/issues/173)) ([903cf88](https://github.com/PBSA/StreamersEdge/commit/903cf88))
* challenge api logged out [#832](https://github.com/PBSA/StreamersEdge/issues/832) ([#174](https://github.com/PBSA/StreamersEdge/issues/174)) ([7a726ef](https://github.com/PBSA/StreamersEdge/commit/7a726ef))
* constraint description length in swagger docs for ReportUser ([#176](https://github.com/PBSA/StreamersEdge/issues/176)) ([18c484c](https://github.com/PBSA/StreamersEdge/commit/18c484c))
* fixing migration-undo ([#178](https://github.com/PBSA/StreamersEdge/issues/178)) ([00033a5](https://github.com/PBSA/StreamersEdge/commit/00033a5))
* modifying getChallengesApi [#866](https://github.com/PBSA/StreamersEdge/issues/866) ([#184](https://github.com/PBSA/StreamersEdge/issues/184)) ([acf9448](https://github.com/PBSA/StreamersEdge/commit/acf9448))
* return error route for failure in oauth ([#189](https://github.com/PBSA/StreamersEdge/issues/189)) ([a0b2956](https://github.com/PBSA/StreamersEdge/commit/a0b2956))
* strm-742: proper redirect after social media register ([#175](https://github.com/PBSA/StreamersEdge/issues/175)) ([a8cf70e](https://github.com/PBSA/StreamersEdge/commit/a8cf70e))
* strm-875: add timeFormat to profile validator ([#182](https://github.com/PBSA/StreamersEdge/issues/182)) ([1aca364](https://github.com/PBSA/StreamersEdge/commit/1aca364))
* strm-897 change response for change-email api to match swagger docs ([#188](https://github.com/PBSA/StreamersEdge/issues/188)) ([d88f011](https://github.com/PBSA/StreamersEdge/commit/d88f011))
* strm-900: unset session.newUser on social media login ([#187](https://github.com/PBSA/StreamersEdge/issues/187)) ([c902ca9](https://github.com/PBSA/StreamersEdge/commit/c902ca9))


### Features

* create challenge changes ([#181](https://github.com/PBSA/StreamersEdge/issues/181)) ([94a3a90](https://github.com/PBSA/StreamersEdge/commit/94a3a90))
* strm-869: new challenge join api ([#180](https://github.com/PBSA/StreamersEdge/issues/180)) ([9eff3a9](https://github.com/PBSA/StreamersEdge/commit/9eff3a9))
* strm-872  changes to twitch api and strm-874 remove set invitation ([#179](https://github.com/PBSA/StreamersEdge/issues/179)) ([88435e4](https://github.com/PBSA/StreamersEdge/commit/88435e4))
* strm-878: model changes ([#177](https://github.com/PBSA/StreamersEdge/issues/177)) ([fb0855d](https://github.com/PBSA/StreamersEdge/commit/fb0855d))



## [0.3.0](https://github.com/PBSA/StreamersEdge/compare/v0.2.0...v0.3.0) (2019-11-27)


### Bug Fixes

* challenge api logged out [#832](https://github.com/PBSA/StreamersEdge/issues/832) ([#173](https://github.com/PBSA/StreamersEdge/issues/173)) ([903cf88](https://github.com/PBSA/StreamersEdge/commit/903cf88))
* challenge api logged out [#832](https://github.com/PBSA/StreamersEdge/issues/832) ([#174](https://github.com/PBSA/StreamersEdge/issues/174)) ([7a726ef](https://github.com/PBSA/StreamersEdge/commit/7a726ef))
* constraint description length in swagger docs for ReportUser ([#176](https://github.com/PBSA/StreamersEdge/issues/176)) ([18c484c](https://github.com/PBSA/StreamersEdge/commit/18c484c))
* fixing migration-undo ([#178](https://github.com/PBSA/StreamersEdge/issues/178)) ([00033a5](https://github.com/PBSA/StreamersEdge/commit/00033a5))
* strm-742: proper redirect after social media register ([#175](https://github.com/PBSA/StreamersEdge/issues/175)) ([a8cf70e](https://github.com/PBSA/StreamersEdge/commit/a8cf70e))
* strm-875: add timeFormat to profile validator ([#182](https://github.com/PBSA/StreamersEdge/issues/182)) ([1aca364](https://github.com/PBSA/StreamersEdge/commit/1aca364))


### Features

* create challenge changes ([#181](https://github.com/PBSA/StreamersEdge/issues/181)) ([94a3a90](https://github.com/PBSA/StreamersEdge/commit/94a3a90))
* strm-869: new challenge join api ([#180](https://github.com/PBSA/StreamersEdge/issues/180)) ([9eff3a9](https://github.com/PBSA/StreamersEdge/commit/9eff3a9))
* strm-872  changes to twitch api and strm-874 remove set invitation ([#179](https://github.com/PBSA/StreamersEdge/issues/179)) ([88435e4](https://github.com/PBSA/StreamersEdge/commit/88435e4))
* strm-878: model changes ([#177](https://github.com/PBSA/StreamersEdge/issues/177)) ([fb0855d](https://github.com/PBSA/StreamersEdge/commit/fb0855d))



## 0.2.0 (2019-11-20)


### Bug Fixes

* (STRM-547) sign up api throwing error 500 ([#97](https://github.com/PBSA/StreamersEdge/issues/97)) ([d553e88](https://github.com/PBSA/StreamersEdge/commit/d553e88))
* 500 server error and  426,409,452 ([#86](https://github.com/PBSA/StreamersEdge/issues/86)) ([395a345](https://github.com/PBSA/StreamersEdge/commit/395a345))
* add callback to frontend url for emails with tokens ([#145](https://github.com/PBSA/StreamersEdge/issues/145)) ([075bfaa](https://github.com/PBSA/StreamersEdge/commit/075bfaa))
* add joined users to challenge response ([#131](https://github.com/PBSA/StreamersEdge/issues/131)) ([b0de68e](https://github.com/PBSA/StreamersEdge/commit/b0de68e))
* add model in require ([#133](https://github.com/PBSA/StreamersEdge/issues/133)) ([b58f037](https://github.com/PBSA/StreamersEdge/commit/b58f037))
* add openapi specs yaml file ([#61](https://github.com/PBSA/StreamersEdge/issues/61)) ([582f8d3](https://github.com/PBSA/StreamersEdge/commit/582f8d3))
* add user data to challenges and update swagger doc ([#136](https://github.com/PBSA/StreamersEdge/issues/136)) ([7be747a](https://github.com/PBSA/StreamersEdge/commit/7be747a))
* added description for all properties in the challenge create method ([#67](https://github.com/PBSA/StreamersEdge/issues/67)) ([5f0efd8](https://github.com/PBSA/StreamersEdge/commit/5f0efd8))
* added email tld check while signing up ([#71](https://github.com/PBSA/StreamersEdge/issues/71)) ([71a46d8](https://github.com/PBSA/StreamersEdge/commit/71a46d8))
* added migrations seeds factory sequelize-cli ([#481](https://github.com/PBSA/StreamersEdge/issues/481), [#438](https://github.com/PBSA/StreamersEdge/issues/438), [#484](https://github.com/PBSA/StreamersEdge/issues/484)) ([#72](https://github.com/PBSA/StreamersEdge/issues/72)) ([4a84f1a](https://github.com/PBSA/StreamersEdge/commit/4a84f1a))
* added nvmrc, docker_dependencies, and ticket fixing ([#438](https://github.com/PBSA/StreamersEdge/issues/438)) ([#70](https://github.com/PBSA/StreamersEdge/issues/70)) ([f742d6a](https://github.com/PBSA/StreamersEdge/commit/f742d6a))
* adding game api [#93](https://github.com/PBSA/StreamersEdge/issues/93) ([#115](https://github.com/PBSA/StreamersEdge/issues/115)) ([9fd990e](https://github.com/PBSA/StreamersEdge/commit/9fd990e))
* adding migrations for extra column ([#77](https://github.com/PBSA/StreamersEdge/issues/77)) ([2d4eb48](https://github.com/PBSA/StreamersEdge/commit/2d4eb48))
* adding search by bounty and date [#678](https://github.com/PBSA/StreamersEdge/issues/678) ([#159](https://github.com/PBSA/StreamersEdge/issues/159)) ([1a3f7dc](https://github.com/PBSA/StreamersEdge/commit/1a3f7dc))
* allow null ID & callback url [#329](https://github.com/PBSA/StreamersEdge/issues/329) ([#125](https://github.com/PBSA/StreamersEdge/issues/125)) ([1d16610](https://github.com/PBSA/StreamersEdge/commit/1d16610))
* allow using other than core assets for challenge creation ([#150](https://github.com/PBSA/StreamersEdge/issues/150)) ([13d272e](https://github.com/PBSA/StreamersEdge/commit/13d272e))
* amount in depositOp should be shifted by precision before comparing ([#151](https://github.com/PBSA/StreamersEdge/issues/151)) ([47507fd](https://github.com/PBSA/StreamersEdge/commit/47507fd))
* check if peerplays account exists with the username at sign up ([#116](https://github.com/PBSA/StreamersEdge/issues/116)) ([9e33cb4](https://github.com/PBSA/StreamersEdge/commit/9e33cb4))
* confirm email now logs in the user and returns user in response ([#119](https://github.com/PBSA/StreamersEdge/issues/119)) ([0d10397](https://github.com/PBSA/StreamersEdge/commit/0d10397))
* donate transaction api missing ([#95](https://github.com/PBSA/StreamersEdge/issues/95)) ([6ea95c7](https://github.com/PBSA/StreamersEdge/commit/6ea95c7))
* error in populate streams api ([#158](https://github.com/PBSA/StreamersEdge/issues/158)) ([5224661](https://github.com/PBSA/StreamersEdge/commit/5224661))
* error on peerplays account linking ([#144](https://github.com/PBSA/StreamersEdge/issues/144)) ([3123f1c](https://github.com/PBSA/StreamersEdge/commit/3123f1c))
* error structuring ([#100](https://github.com/PBSA/StreamersEdge/issues/100)) ([3c1b764](https://github.com/PBSA/StreamersEdge/commit/3c1b764))
* error while logging in with peerplays account ([#124](https://github.com/PBSA/StreamersEdge/issues/124)) ([ca10c20](https://github.com/PBSA/StreamersEdge/commit/ca10c20))
* failing test for patch profile peerplays account name ([#139](https://github.com/PBSA/StreamersEdge/issues/139)) ([b533c73](https://github.com/PBSA/StreamersEdge/commit/b533c73))
* failing tests ([#152](https://github.com/PBSA/StreamersEdge/issues/152)) ([cfde197](https://github.com/PBSA/StreamersEdge/commit/cfde197))
* fix front urls ([#46](https://github.com/PBSA/StreamersEdge/issues/46)) ([308797e](https://github.com/PBSA/StreamersEdge/commit/308797e))
* fixing test cases 561 ([#102](https://github.com/PBSA/StreamersEdge/issues/102)) ([0d8fcd1](https://github.com/PBSA/StreamersEdge/commit/0d8fcd1))
* getForChallenge doesn't exist ([#132](https://github.com/PBSA/StreamersEdge/issues/132)) ([adb7643](https://github.com/PBSA/StreamersEdge/commit/adb7643))
* login user after reset password ([#164](https://github.com/PBSA/StreamersEdge/issues/164)) ([6ecea70](https://github.com/PBSA/StreamersEdge/commit/6ecea70))
* login using youtube & update twitch 534 ([#99](https://github.com/PBSA/StreamersEdge/issues/99)) ([bfe42aa](https://github.com/PBSA/StreamersEdge/commit/bfe42aa))
* made test cases independent of database state ([#84](https://github.com/PBSA/StreamersEdge/issues/84)) ([9d55f1a](https://github.com/PBSA/StreamersEdge/commit/9d55f1a))
* npm-shrinkwrap merge conflict ([#91](https://github.com/PBSA/StreamersEdge/issues/91)) ([80cb811](https://github.com/PBSA/StreamersEdge/commit/80cb811))
* patch twitchUsername and googlename not working ([#107](https://github.com/PBSA/StreamersEdge/issues/107)) ([01f3310](https://github.com/PBSA/StreamersEdge/commit/01f3310))
* peerplays account login with invalid password ([#167](https://github.com/PBSA/StreamersEdge/issues/167)) ([ac77729](https://github.com/PBSA/StreamersEdge/commit/ac77729))
* possible fix-crypto undefined error on dev, update node, audit fix ([#138](https://github.com/PBSA/StreamersEdge/issues/138)) ([26a9083](https://github.com/PBSA/StreamersEdge/commit/26a9083))
* refactor challenge code to be stateless and fixed invite error ([#73](https://github.com/PBSA/StreamersEdge/issues/73)) ([ee71dc0](https://github.com/PBSA/StreamersEdge/commit/ee71dc0))
* restricting information being return in search api ([#85](https://github.com/PBSA/StreamersEdge/issues/85)) ([30103d4](https://github.com/PBSA/StreamersEdge/commit/30103d4))
* return only username and avatar for creator ([#146](https://github.com/PBSA/StreamersEdge/issues/146)) ([535203c](https://github.com/PBSA/StreamersEdge/commit/535203c))
* search challenge by name,game [#678](https://github.com/PBSA/StreamersEdge/issues/678) ([#157](https://github.com/PBSA/StreamersEdge/issues/157)) ([67ee51e](https://github.com/PBSA/StreamersEdge/commit/67ee51e))
* searching username instead of peerplay name ([#80](https://github.com/PBSA/StreamersEdge/issues/80)) ([6a66c7a](https://github.com/PBSA/StreamersEdge/commit/6a66c7a))
* server side error when email with caps already exists ([#129](https://github.com/PBSA/StreamersEdge/issues/129)) ([6a92e02](https://github.com/PBSA/StreamersEdge/commit/6a92e02))
* setInvitation all/none [#656](https://github.com/PBSA/StreamersEdge/issues/656) ([#128](https://github.com/PBSA/StreamersEdge/issues/128)) ([f27ae10](https://github.com/PBSA/StreamersEdge/commit/f27ae10))
* skip sending notifications if subscription data is missing ([#153](https://github.com/PBSA/StreamersEdge/issues/153)) ([a85a1af](https://github.com/PBSA/StreamersEdge/commit/a85a1af))
* strm-16: joined users table fixes ([#121](https://github.com/PBSA/StreamersEdge/issues/121)) ([ecd93d9](https://github.com/PBSA/StreamersEdge/commit/ecd93d9))
* strm-200: remove leftover ppyAmount from paypal redemption model ([#130](https://github.com/PBSA/StreamersEdge/issues/130)) ([7467f1a](https://github.com/PBSA/StreamersEdge/commit/7467f1a))
* strm-437 email verification ([#101](https://github.com/PBSA/StreamersEdge/issues/101)) ([3583a02](https://github.com/PBSA/StreamersEdge/commit/3583a02))
* strm-440 yaml errors ([#93](https://github.com/PBSA/StreamersEdge/issues/93)) ([b21b7bf](https://github.com/PBSA/StreamersEdge/commit/b21b7bf))
* strm-565: limit username length to 60 chars and add test ([#103](https://github.com/PBSA/StreamersEdge/issues/103)) ([118dd7c](https://github.com/PBSA/StreamersEdge/commit/118dd7c))
* strm-590: reset minInvitationBounty when invitation status is none ([#141](https://github.com/PBSA/StreamersEdge/issues/141)) ([22a2f34](https://github.com/PBSA/StreamersEdge/commit/22a2f34))
* strm-602: fix error with duplicate empty string twitchUserName ([#108](https://github.com/PBSA/StreamersEdge/issues/108)) ([4df6b42](https://github.com/PBSA/StreamersEdge/commit/4df6b42))
* strm-603: confirm email fixes ([#109](https://github.com/PBSA/StreamersEdge/issues/109)) ([96c8d6b](https://github.com/PBSA/StreamersEdge/commit/96c8d6b))
* strm-608: update vulnerable dependencies ([#110](https://github.com/PBSA/StreamersEdge/issues/110)) ([9b214fe](https://github.com/PBSA/StreamersEdge/commit/9b214fe))
* strm-687: remove private user details from challenge joined users ([#143](https://github.com/PBSA/StreamersEdge/issues/143)) ([aaf3cf5](https://github.com/PBSA/StreamersEdge/commit/aaf3cf5))
* strm-719: add conditions property to returned challenges ([#142](https://github.com/PBSA/StreamersEdge/issues/142)) ([3d55628](https://github.com/PBSA/StreamersEdge/commit/3d55628))
* strm-741: notifications rework ([#149](https://github.com/PBSA/StreamersEdge/issues/149)) ([53142ca](https://github.com/PBSA/StreamersEdge/commit/53142ca))
* strm-755: change ppyAmount to double ([#162](https://github.com/PBSA/StreamersEdge/issues/162)) ([a16c106](https://github.com/PBSA/StreamersEdge/commit/a16c106))
* strm-765: validate challenge conditions ([#161](https://github.com/PBSA/StreamersEdge/issues/161)) ([119956e](https://github.com/PBSA/StreamersEdge/commit/119956e))
* strm-774: fix swagger docs for /profile/league-of-legends/realms ([#160](https://github.com/PBSA/StreamersEdge/issues/160)) ([d193178](https://github.com/PBSA/StreamersEdge/commit/d193178))
* strm-775: create challenge before peerplays tx ([#170](https://github.com/PBSA/StreamersEdge/issues/170)) ([cb49107](https://github.com/PBSA/StreamersEdge/commit/cb49107))
* strm-778: league of legends games job query fix ([#165](https://github.com/PBSA/StreamersEdge/issues/165)) ([04abd79](https://github.com/PBSA/StreamersEdge/commit/04abd79))
* strm-778: paypal payouts fix ([#168](https://github.com/PBSA/StreamersEdge/issues/168)) ([101c4ed](https://github.com/PBSA/StreamersEdge/commit/101c4ed))
* strm-784: fix swagger docs for challenge wins api ([#169](https://github.com/PBSA/StreamersEdge/issues/169)) ([f36c3cc](https://github.com/PBSA/StreamersEdge/commit/f36c3cc))
* strm-837: report user changes ([#171](https://github.com/PBSA/StreamersEdge/issues/171)) ([0316b27](https://github.com/PBSA/StreamersEdge/commit/0316b27))
* test cases migrations scripts readme ([#493](https://github.com/PBSA/StreamersEdge/issues/493)) ([#75](https://github.com/PBSA/StreamersEdge/issues/75)) ([81932c6](https://github.com/PBSA/StreamersEdge/commit/81932c6))
* twitch avatar and email not saving in db at sign up ([#117](https://github.com/PBSA/StreamersEdge/issues/117)) ([a2462f8](https://github.com/PBSA/StreamersEdge/commit/a2462f8))
* twitchusername, googlename and facebook not showing ([#66](https://github.com/PBSA/StreamersEdge/issues/66)) ([d40685e](https://github.com/PBSA/StreamersEdge/commit/d40685e))
* update profile api can remove user's email ([#154](https://github.com/PBSA/StreamersEdge/issues/154)) ([45bbdda](https://github.com/PBSA/StreamersEdge/commit/45bbdda))
* upload avatar 452 ([#90](https://github.com/PBSA/StreamersEdge/issues/90)) ([abcb7d8](https://github.com/PBSA/StreamersEdge/commit/abcb7d8))
* using firstname on facebook and facebook id to create username ([#156](https://github.com/PBSA/StreamersEdge/issues/156)) ([847b7ad](https://github.com/PBSA/StreamersEdge/commit/847b7ad))
* validation fixes ([#76](https://github.com/PBSA/StreamersEdge/issues/76)) ([bb73b6f](https://github.com/PBSA/StreamersEdge/commit/bb73b6f))
* **strm-405:** further limitations on profile images ([#88](https://github.com/PBSA/StreamersEdge/issues/88)) ([cdb68f7](https://github.com/PBSA/StreamersEdge/commit/cdb68f7))
* **strm-423:** fixed new issue ([#92](https://github.com/PBSA/StreamersEdge/issues/92)) ([8500642](https://github.com/PBSA/StreamersEdge/commit/8500642))
* **strm-464:** [Back End] SE_API ([#79](https://github.com/PBSA/StreamersEdge/issues/79)) ([2836ed0](https://github.com/PBSA/StreamersEdge/commit/2836ed0))
* **token issue:** added confirion for expire ([#78](https://github.com/PBSA/StreamersEdge/issues/78)) ([3f1f0a1](https://github.com/PBSA/StreamersEdge/commit/3f1f0a1))
* validation for changInvitation [#655](https://github.com/PBSA/StreamersEdge/issues/655) ([#126](https://github.com/PBSA/StreamersEdge/issues/126)) ([84d5c6d](https://github.com/PBSA/StreamersEdge/commit/84d5c6d))


### Features

* strm-16: add joined info on challenge results ([#123](https://github.com/PBSA/StreamersEdge/issues/123)) ([dc8b693](https://github.com/PBSA/StreamersEdge/commit/dc8b693))
* strm-16: join challenge transactions ([#127](https://github.com/PBSA/StreamersEdge/issues/127)) ([51ef81f](https://github.com/PBSA/StreamersEdge/commit/51ef81f))
* strm-200: redemptions/ paypal payouts ([#120](https://github.com/PBSA/StreamersEdge/issues/120)) ([b0b7dcc](https://github.com/PBSA/StreamersEdge/commit/b0b7dcc))
* strm-41, strm-46, strm-74, strm-265 ([#118](https://github.com/PBSA/StreamersEdge/issues/118)) ([1eefb24](https://github.com/PBSA/StreamersEdge/commit/1eefb24))
* strm-522: sign peerplays transactions on the backend ([#106](https://github.com/PBSA/StreamersEdge/issues/106)) ([ba66233](https://github.com/PBSA/StreamersEdge/commit/ba66233))
* strm-524: create peerplays account on sign-up with social media ([#104](https://github.com/PBSA/StreamersEdge/issues/104)) ([d2fbfa3](https://github.com/PBSA/StreamersEdge/commit/d2fbfa3))
* strm-564 peerplays account login, linking and unlinking ([#122](https://github.com/PBSA/StreamersEdge/issues/122)) ([c787ac6](https://github.com/PBSA/StreamersEdge/commit/c787ac6))
* strm-587: link paypal account ([#114](https://github.com/PBSA/StreamersEdge/issues/114)) ([e8e24af](https://github.com/PBSA/StreamersEdge/commit/e8e24af))
* strm-590: implement minimum invitation bounty ([#111](https://github.com/PBSA/StreamersEdge/issues/111)) ([904fc02](https://github.com/PBSA/StreamersEdge/commit/904fc02))
* strm-660: league of legends integration ([#137](https://github.com/PBSA/StreamersEdge/issues/137)) ([d804588](https://github.com/PBSA/StreamersEdge/commit/d804588))
* strm-758: delete old user avatar ([#163](https://github.com/PBSA/StreamersEdge/issues/163)) ([18ad0fe](https://github.com/PBSA/StreamersEdge/commit/18ad0fe))
* strm-784: get won challenges api ([#166](https://github.com/PBSA/StreamersEdge/issues/166)) ([3b8036f](https://github.com/PBSA/StreamersEdge/commit/3b8036f))
